# # interger

# a = 10
# b = a

# print(a)

# strings

# sentence = "to be or not to be"
# print(type(sentence))

# print(sentence.find("be"))
# print(sentence.replace("be", "me"))

#upper
#lower
#split - splits a string into a list

# print(sentence.upper())
# print(sentence.split())

# sentence2 = "party"
# print(sentence2.split())
# my_list = sentence2.split()

# # join
# back_to_string = '&'.join(my_list)
# print(back_to_string)

# Conditionals in Python

# if statement - checking for truth

# age = 22

# if age > 21:
#     print("you can rent a car")
# else: print('nope, you are not old enough')

# my_input = input("Enter how often you party?: -")
# print(f"User parties on {my_input}")

# Exercise

# Capture the user input for colors that are green, yellow, red
# print what the user entered


# write an if...elif...else statement that prints out one of the following messages:

# if green is entered, print the message "Go!"
# if yellow - print "Slow Down!"
# if red - "Stop!"
# if anything - print whatever you want 

# user_input = input("Please enter green, yellow or red")

# print(user_input)

# if user_input == "green".lower():
#     print("Go!")
# elif user_input == "yellow".lower():
#     print("Slow Down!")
# elif user_input == "red".lower():
#     print("Stop!")
# else:
#     print("Don't quit your day job")

# Exercise 2: 

# Build an age guesser 

# ask the user to input their birth year 
# And print their age.

# user_birth_year = input("enter your birth year")
# user_age = 2023 - int(user_birth_year)

# print(f"You are {user_age} years old this year")

# Exercises 

# 1. Write a program to check whether a number entered is three digit number or not. 

# 2. Write a program to find the lowest number out of two numbers inputed by a user. 

# 3. Write a program to check whether a number (inputed by the user) is divisible by both 2 and 3.

# 1
# num = 10

# if len(str(num)) == 3:
#     print("Number is three digits")
# else:
#     print("Number is not three digits")

#2
# num_one = input("please enter number")
# num_two = input("please enter different number")

# if num_one > num_two:
#     print(f"{num_two} is lowest")
# else:
#     print(f"{num_one} is lowest")

#3

# user_num = int(input("please enter number"))
# if type(user_num) != int:
#     print("try again")
# elif user_num % 2 == 0 and user_num % 3 == 0:
#     print(f"{user_num} is is divisible by both 2 and 3")
# else:
#     print (f"{user_num} is not is divisible by both 2 and 3")

# 4. Accept as input three sides of a triangle and check whether it is an equilateral, isosceles, or scalene triangle
#isosceles two equal sides
#scalene all different
# side_one = input("please enter a number")
# side_two = input("please enter a number")
# side_three = input("please enter a number")
# if side_one != side_two and side_one != side_three:
#     print("you're sides are all different, Scalene!")
# elif side_one == side_two and side_one == side_three:
#     print("all sides of you are equal, Equalateral!")
# elif side_one == side_two or side_one == side_three or side_three == side_two:
#     print("two of your sides are equal, Isosceles!")
# else:
#     print("get outta here trapezoid!")

# 5. Accept three numbers from the user and display the second largest number 
# num_one = input("please enter a number")
# num_two = input("please enter a number")
# num_three = input("please enter a number")
# sorted_nums = sorted([num_one, num_two, num_three])
# print(f"{sorted_nums[1]} is the second largest number you entered")


# 6. Write a program to convert temperatures to and from Celsius and Fahrenheit - take user input - "Input the temperature you like to convert? (e.g, 45F, 102C, etc..") 

# sample output will look like 
# Input the  temperature you like to convert? (e.g., 45F, 102C etc.) : 104f                                     
# The temperature in Celsius is 40 degrees. 

temp = input("please enter temperature and letter of temp NO Spaces!")
temp_list = [*temp] # turn input string into list
print(temp_list)
temp_unit = temp_list.pop() # store f or c and pop off list
print(temp_unit)
temp_num = ''.join(temp_list) # turn string into numbers
print(temp_num)

if temp_unit.lower() =='f':
    convert_to_c = (int(temp_num) * 9/5) + 32
    print(f"Temp is {convert_to_c} degrees Celsius")
elif temp_unit.lower() == 'c':
    convert_to_f = (int(temp_num) - 32) * 5/9
    print (f"Temp is {convert_to_f} degrees Farenheit")
else:
    print("what the heck is Kelvin?")
# 7. Write a Python program to calculate a dog's age in dog years.
# Note: For the first two years, a dog year is equal to 10.5 human years. After that, each dog year equals 4 human years.

# sample output: 
# Input a dog's age in human years: 12                                                                          
# The dog's age in dog's years is 61   

# 8. Write a Python program to check whether an alphabet is a vowel or consonant.

# 9.Write a Python program to convert a month name to a number of days.

# sample output: 
# List of months: January, February, March, April, May, June, July, August, September, October, November, December                                                                                                            
# Input the name of Month: April                                                                                
# No. of days: 30 days  

# 10. Write a Python program to display the astrological sign for a given date of birth.

# sample output:
# Input birthday: 18                                                                                            
# Input month of birth (e.g. march, july etc): january                                                          
# Your Astrological sign is : Capricorn

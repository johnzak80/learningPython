# what is core of programming


#range() allows me to generate numbers through a for loop in some range i give
# range are a sequence type
#range represents an immutable sequence of numbers
# commonly used for looping a specific number of times in for loops

# for num in range(45, 100):
#     print(num)

    # range can aslo be used to create lists and tuples

# my_list = list(range(10))
#     print(my_list)

# # creat elist of odd numbers ranging from 3-99
# odd_nums_list = list(range(3, 100, 2))
#     print(odd_nums_list)

# for num in range(5, 0, -1):
#     print(num)

my_string = "party"

for char in my_string:
    print(char.upper())

for index in range (len("Hello")):
    print(index)